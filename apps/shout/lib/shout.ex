defmodule Shout do
  @moduledoc """
  Documentation for `Shout`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Shout.hello()
      :world

  """
  def hello do
    :world
  end
end
