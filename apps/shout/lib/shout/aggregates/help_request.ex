defmodule Shout.Aggregates.HelpRequest do
  alias Shout.Commands.{GiveHelp, HelpMe}
  alias Shout.Events.{HelpGiven, HelpRequested}
  alias Shout.Aggregates.HelpRequest

  use TypedStruct
  import Logger

  @derive Jason.Encoder
  typedstruct do
    field :id, String.t(), enforce: true
    field :from, String.t(), enforce: true, default: "__from__"
    field :to, String.t(), enforce: true, default: "__to__"
    field :data, struct(), enforce: false, default: %{}
    field :responses, [tuple()], enforce: false, default: []
  end

  def execute(%HelpRequest{id: nil}, %HelpMe{} = event) do
    %HelpRequested{
      help_uuid: event.help_uuid,
      from: event.from,
      to: event.to,
      data: event.data
    }
  end

  def execute(%HelpRequest{id: _id}, %HelpMe{}) do
    {:error, :already_exists}
  end

  def execute(%HelpRequest{id: nil} = _state, %GiveHelp{} = _event) do
    Logger.warn("got no state for GiveHelp")
  end

  def execute(%HelpRequest{id: _id}, %GiveHelp{} = event) do
    sent_event = %HelpGiven{
      from: event.from,
      in_reply_to: event.in_reply_to,
      accepted?: event.accepted?
    }
    sent_event
  end

  # state mutators
  def apply(%HelpRequest{} = state, %HelpRequested{} = event) do
    %HelpRequest{state |
                 id: event.help_uuid,
                 from: event.from,
                 to: event.to,
                 data: event.data,
                 responses: []}
  end

  def apply(%HelpRequest{} = state, %HelpGiven{} = event) do
    %HelpRequest{state | responses: state.responses ++ [
      %{
        from: event.from,
        accepted?: event.accepted?
      }
    ]}
  end
  
 end
