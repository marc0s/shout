defmodule Shout.EventApplication do
  use Commanded.Application, otp_app: :shout

  @behaviour Shout.EventApplication.Behaviour
  alias Shout.Routers.Help

  router(Help)

  defmodule Behaviour do
    @callback help_me(%{}) :: nil
    @callback answer(%{}) :: nil
  end

  # Public API
  def help_me(%{} = request) do
    IO.inspect(request)
  end

  def answer(%{} = response) do
    IO.inspect(response)
  end
end
