defmodule Shout.Events.HelpGiven do
  use TypedStruct

  @derive Jason.Encoder
  typedstruct do
    field :from, String.t(), enforce: true
    field :in_reply_to, String.t(), enforce: true
    field :accepted?, boolean(), enforce: false, default: false
  end

end
