defmodule Shout.Events.HelpRequested do
  use TypedStruct

  @derive Jason.Encoder
  typedstruct do
    field :help_uuid, String.t(), enforce: true
    field :from, String.t(), enforce: true
    field :to, String.t(), enforce: true
    field :data, struct(), enforce: false
  end
end
