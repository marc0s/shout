defmodule Shout.Routers.Help do
  use Commanded.Commands.Router
  alias Shout.Commands.{GiveHelp, HelpMe}
  alias Shout.Aggregates.HelpRequest

  dispatch HelpMe, to: HelpRequest, identity: :help_uuid
  dispatch GiveHelp, to: HelpRequest, identity: :in_reply_to
end
