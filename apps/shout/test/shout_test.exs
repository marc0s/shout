defmodule ShoutTest do
  use Shout.DataCase
  use HelpMeBuilders
  

  describe "requesting help" do
    test "ensure HelpRequested event is published for HelpMe command" do
      command = helpme_command()
      :ok = Shout.EventApplication.dispatch(command)

      assert_receive_event(Shout.EventApplication, HelpRequested, fn event ->
        assert event.help_uuid
        assert event.from == "some from"
        assert event.to == "some to"
        assert event.data == %{department: "kids"}
      end)
    end

    test "ensure HelpMe command cannot be issued for the same help_uuid twice" do
      command = helpme_command()
      :ok = Shout.EventApplication.dispatch(command)
      command = %HelpMe{command |  from: "some other from", to: "some other to", data: %{department: "home"}}
      assert Shout.EventApplication.dispatch(command) == {:error, :already_exists}
    end
  end

  describe "giving help" do
    setup [:create_helpme_request]

    test "ensure GiveHelp command fires the HelpGiven event with the proper payload", %{help_uuid: help_uuid} do
      command = givehelp_command_accepting(help_uuid)
      :ok = Shout.EventApplication.dispatch(command)
      assert_receive_event(Shout.EventApplication, HelpGiven, fn event ->
        assert event.in_reply_to == help_uuid
        assert event.accepted? == true
        assert event.from == "helper"
      end)
    end
  end

  defp create_helpme_request(context) do
    command = helpme_command()
    :ok = Shout.EventApplication.dispatch(command)
    wait_for_event(Shout.EventApplication, HelpRequested)
    {:ok, Map.put(context, :help_uuid, command.help_uuid)}
  end

end
