defmodule HelpMeBuilders do
  defmacro __using__(_options) do
    quote do
        alias Shout.Commands.{GiveHelp, HelpMe}
        alias Shout.Events.{HelpGiven, HelpRequested}

        import HelpMeBuilders, only: :functions
    end
  end

  alias Shout.Commands.{GiveHelp, HelpMe}
  alias Shout.Events.{HelpGiven, HelpRequested}

  def helpme_command() do
    %HelpMe{
      help_uuid: UUID.uuid4(),
      from: "some from",
      to: "some to",
      data: %{department: "kids"}
    }
  end

  def givehelp_command_accepting(in_reply_to) do
    %GiveHelp{
      from: "helper",
      in_reply_to: in_reply_to,
      accepted?: true
    }
  end

  def givehelp_command_rejecting(in_reply_to) do
    %GiveHelp{givehelp_command_accepting(in_reply_to) | accepted?: false}
  end
end
