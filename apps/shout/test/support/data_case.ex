defmodule Shout.DataCase do
  use ExUnit.CaseTemplate

  using do
    quote do
      import Commanded.Assertions.EventAssertions
    end
  end

  setup  do
    {:ok, _} = Application.ensure_all_started(:shout)
    on_exit(fn ->
      :ok = Application.stop(:shout)
      :ok = Application.stop(:commanded)
    end)
  end
end
