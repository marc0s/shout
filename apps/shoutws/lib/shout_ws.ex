defmodule ShoutWs do
  @moduledoc """
  Documentation for `ShoutWs`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> ShoutWs.hello()
      :world

  """
  def hello do
    :world
  end
end
