defmodule ShoutWs.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  @ws_path Application.get_env(:shoutws, :path)
  @ws_port Application.get_env(:shoutws, :port, 4444)

  use Application

  def start(_type, _args) do
    children = [
      {Plug.Cowboy, scheme: :http, plug: ShoutWs.Router, options: [
          port: @ws_port,
          dispatch: dispatch()
        ]
      },
      {Registry, keys: :duplicate, name: Registry.ShoutWs}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: ShoutWs.Supervisor]
    Supervisor.start_link(children, opts)
  end

  defp dispatch do
    [
      {:_, [
          {@ws_path, ShoutWs.SocketHandler, []},
          {:_, Plug.Cowboy.Handler, {ShoutWs.Router, []}}
        ]}
    ]
  end
end
