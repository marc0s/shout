defmodule ShoutWs.Dispatcher do
  @moduledoc """
  Delivers incoming help requests and responses to the domain.
  """
  require Logger

  @shout_app Application.get_env(:shoutws, :shoutApp)

  alias ShoutWs.Request
  alias ShoutWs.Response

  @behaviour ShoutWs.Dispatcher.Behaviour

  defmodule Behaviour do
    @callback deliver(Request.t() | Response.t()) :: :ok | :error
  end

  @doc """
  Sends the incoming help request to the domain for handling it
  """
  def deliver(%Request{from: _from, to: _to} = req) do
    @shout_app.help_me(req)
    :ok
  end

  @doc """
  Sends the incoming response to the domain for handling it
  """
  def deliver(%Response{} = res) do
    @shout_app.answer(res)
    :ok
  end

end
