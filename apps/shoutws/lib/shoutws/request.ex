defmodule ShoutWs.Request do
  @moduledoc """
  The struct representing a help request coming from the WS to the Shout domain.
  """

  use TypedStruct

  @derive Jason.Encoder
  @typedoc "A help request"
  typedstruct do
    field :from, String.t(), enforce: true
    field :to, String.t(), enforce: true
    field :data, struct(), enforce: false
  end
end
