defmodule ShoutWs.Response do
  @moduledoc """
  The struct representing a help response coming from the WS to the Shout domain.
  """

  use TypedStruct

  @derive Jason.Encoder
  @typedoc "A help request response"
  typedstruct do
    field :reply_to, String.t(), enforce: true
    field :from, String.t(), enforce: true
    field :accepted?, boolean(), default: true
  end
end
