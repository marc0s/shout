defmodule ShoutWs.Router do
  use Plug.Router

  plug Plug.Logger
  plug :match
  plug :dispatch

  # we don't offer anything via HTTP, just WS over /ws
  match _ do
    send_resp(conn, 200, "Welcome to ShoutWs. Connect to /ws please")
  end
end
