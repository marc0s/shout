defmodule ShoutWs.SocketHandler do
  @behaviour :cowboy_websocket

  @dispatcher Application.get_env(:shoutws, :dispatcher)

  alias ShoutWs.{
    Request,
    Response
  }
  
  require Logger
  
  def init(req, _state) do
    Logger.debug("new client connected at #{req.path}")
    state = %{registry_key: req.peer}
    {:cowboy_websocket, req, state}
  end

  def websocket_init(state) do
    Registry.ShoutWs
    |> Registry.register(state.registry_key, {})
    
    {:ok, state}
  end
  
  def websocket_handle({:text, message}, state) do
    Logger.debug("server websocket_handle text: #{message}")
    case Jason.decode(message, keys: :atoms) do
      {:ok, json} ->
        websocket_handle({:json, json}, state)
      {:error, _} ->
        {:reply, {:text, "your input is not JSON, try again"}, state}
    end
  end

  def websocket_handle({:json, input}, state) do
    Logger.debug("server websocket_handle json: #{inspect input}")

    # case input[:in_reply_to] do
    #   nil -> handle_request(input, state)
    #   _ -> handle_response(input, state)
    # end

    {:reply, {:text, Jason.encode!(input)}, state}
  end

  def websocket_handle({:ping, message}, state) do
    {:reply, {:pong, message}, state}
  end

  def websocket_info({:broadcast, event}, state) do
    {:reply, {:text, Jason.encode!(event)}, state}
  end
  
  def websocket_info({_timeout, _ref, _msg}, req, state) do
    {:reply, {:text, "info"}, req, state}
  end
  
  def websocket_info(_info, _req, state) do
    {:reply, state}
  end

  def terminate(reason, _req, _state) do
    Logger.debug("Client disconnected: (#{inspect reason})")
    :ok
  end

  defp handle_request(input, state) do
    req = struct(Request, input)
    req = %{req | from: state.registry_key}
    @dispatcher.deliver(req)
  end

  defp handle_response(input, state) do
    res = struct(Response, input)
    res = %{res | from: state.registry_key}
    @dispatcher.deliver(res)
    :ok
  end
 end
