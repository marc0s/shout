defmodule ShoutWs.MixProject do
  use Mix.Project

  def project do
    [
      app: :shoutws,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      elixirc_paths: elixirc_paths(Mix.env()),
      lockfile: "../../mix.lock",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {ShoutWs.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:shout, in_umbrella: true},
      {:jason, "~> 1.2"},
      {:mox, "~> 0.5.2"},
      {:plug_cowboy, "~> 2.0"},
      {:typed_struct, "~> 0.1.4"},
      {:websockex, "~> 0.4.0"}
    ]
  end

  defp aliases do
    [
      test: "test --no-start"
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]
end
