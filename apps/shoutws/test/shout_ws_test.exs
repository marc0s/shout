defmodule ShoutWsTest do
  use ExUnit.Case, async: true
  import Mox

  alias ShoutWs.Request
  alias ShoutWs.Response
  alias ShoutWs.Dispatcher
  alias ShoutWs.Client

  @shout_app Application.get_env(:shoutws, :shoutApp)
  @ws_port Application.get_env(:shoutws, :port)

  #setup :verify_on_exit!

  # test "sending non valid messages closes the socket" do

  # end

  # test "sending a well-formed message for help, calls deliver(%Request{})" do
  #   @shout_app
  #   |> expect(:help_me, 1, fn %Request{from: "source", to: "destination"} = _req ->
  #     :ok
  #   end)

  #   assert Dispatcher.deliver(%Request{from: "source", to: "destination"}) == :ok
  # end

  # test "sending a response for a help requests, calls deliver(%Response{})" do
  #   @shout_app
  #   |> expect(:answer, 1, fn %{}=res ->
  #     IO.inspect res
  #     nil
  #   end)
  #   assert Dispatcher.deliver(%Response{from: "me", in_reply_to: "you"}) == :ok
  # end

  test "ws: send request and get the echo back" do
    {:ok, pid} = Client.start_link("ws://localhost:4000/ws", self())
    msg = Jason.encode!(%{msg: "hi there"})
    Client.request(pid, msg)
    assert_receive {:ws_msg_received, ^pid, msg}
  end
end
