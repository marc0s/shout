defmodule ShoutWs.Client do
  use WebSockex
  require Logger
  
  def start_link(url, send_to_pid) do
    WebSockex.start_link(url, __MODULE__, %{send_to_pid: send_to_pid})
  end


  def request(client, message) do
    Logger.info("Sending request: #{inspect message}")
    WebSockex.send_frame(client, {:text, message})
  end
  
  def handle_connect(_conn, state) do
    Logger.info("Connected")
    {:ok, state}
  end
  
  def handle_frame({:text, msg}, %{send_to_pid: pid}=state) do
    Logger.info "Received a message: #{inspect msg}"
    send pid, {:ws_msg_received, self(), msg}
    {:ok, state}
  end

  def handle_cast({:send, {type, msg} = frame}, state) do
    IO.puts "Sending #{type} frame with payload #{inspect msg}"
    {:reply, frame, state}
  end

  def terminate(_reason, _state) do
    IO.puts("terminate")
    exit(:normal)
  end
end
