use Mix.Config

# shout application config
config :shout, event_stores: [Shout.EventStore]
config :shout, Shout.EventStore, serializer: Commanded.Serialization.JsonSerializer
config :shout, Shout.EventApplication,
  consistencty: :eventual,
  event_store: [
    adapter: Commanded.EventStore.Adapters.EventStore,
    event_store: Shout.EventStore
  ],
  pubsub: :local,
  registry: :local

# shoutws application config
config :shoutws,
  dispatcher: ShoutWs.Dispatcher,
  shoutApp: Shout.EventApplication

config :shoutws, port: 4000, path: "/ws"

import_config "#{Mix.env()}.exs"
