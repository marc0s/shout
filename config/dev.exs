use Mix.Config

# shout application config
config :shout, Shout.EventStore,
  serializer: Commanded.Serialization.JsonSerializer,
  username: "shout",
  password: "shout",
  database: "shout_eventstore_dev",
  hostname: "localhost",
  pool_size: 10

# shoutws application config
config :shoutws, dispatcher: ShoutWs.Dispatcher
