use Mix.Config

# shout application config
config :shout, Shout.EventStore,
  serializer: Commanded.Serialization.JsonSerializer,
  username: "shout",
  password: "shout",
  database: "shout_eventstore_test",
  hostname: "localhost",
  pool_size: 10

config :shout, Shout.EventApplication,
  consistency: :strong,
  event_store: [
    # adapter: Commanded.EventStore.Adapters.EventStore,
    adapter: Commanded.EventStore.Adapters.InMemory,
    event_store: Shout.EventStore
  ],
  pub_sub: :local,
  registry: :local

# Print only warnings and errors during test
# config :logger, level: :warn

config :ex_unit, capture_log: true

# shoutws application config
config :shoutws, dispatcher: ShoutWs.Dispatcher.Mock
config :shoutws, shoutApp: ShoutWs.ShoutApp
